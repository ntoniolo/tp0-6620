#include "argParser.h"

const char* nombre_programa;

void imprime_uso() {
    printf("%s [OPTIONS] [file...]\n", nombre_programa);
    printf( "-h, --help                  display this help and exit.\n"
            "-v, --version               display version information and exit.\n"
            "-c, --bytes                 output the last K bytes.\n"
            "-n, --lines                 output the last K lines, instead of the last 10.\n");
}

T_ProgramSettings readOptions (int argc, char** argv, const char* op_cortas, const struct option op_largas[]) {

	T_ProgramSettings settings;
	settings.paramsInCount = 0;
	int siguiente_opcion = 0;

	while (1) {
		/* Llamamos a la función getopt */
		siguiente_opcion = getopt_long(argc, argv, op_cortas, op_largas, NULL);

		if (siguiente_opcion == -1) {
			if (settings.ToDo[0] == 0 && settings.ToDo[1] == 0 && settings.ToDo[2] == 0 && settings.ToDo[3] == 0) {
				settings.ToDo[0] = 1;
				settings.quantity = 10;
			}
			break; /* No hay más opciones. Rompemos el bucle */
		}

		switch (siguiente_opcion) {
			case 'h' : /* -h o --help */
				imprime_uso();
				exit(EXIT_SUCCESS);

			case 'v' : /* -v o --verbose */
				printf("Version 1.0 de Tail\n");
				exit(1);

			case 'n' : /* -n ó --lines */
			    settings.quantity = 0;
				if (strlen(optarg) >= 2 && optarg[0] == '+') {
					if (optarg[1] != ' ') {
						settings.ToDo[1] = 1;
						settings.quantity = atoi(optarg+1);
					}
				} else {
					settings.ToDo[0] = 1;
					settings.quantity = atoi(optarg);
				}
			    if(settings.quantity <= 0) {
					fprintf(stderr, "La cantidad de lineas a leer debe ser numerica\n");
					exit(1);
				}
				break;

			case 'c' : /* -c ó --bytes */
			    settings.quantity = 0;
				if (strlen(optarg) >= 2 && optarg[0] == '+') {
					if (optarg[1] != ' ') {
						settings.ToDo[3] = 1;
						settings.quantity = atoi(optarg+1);
					}
				} else {
					settings.ToDo[2] = 1;
					settings.quantity = atoi(optarg);
				}
			    if(settings.quantity <= 0) {
					fprintf(stderr, "La cantidad de bytes a leer debe ser numerica\n");
					exit(1);
				}
				break;

			case '?' : /* opción no valida */
				imprime_uso(); /* código de salida 1 */
				exit(1);

			case -1 : /* No hay más opciones */
				break;

			default : /* Algo más? No esperado. Abortamos */
				abort();
		}
	}

	if (optind < argc) {
		while (optind < argc && settings.paramsInCount < MAX_PARAMS_IN) {
			settings.paramsIn[settings.paramsInCount++] = argv[optind++];
		}
	}

	return settings;
}


T_ProgramSettings parseArguments (int argc, char** argv) {

	/* Una cadena que lista las opciones cortas válidas */
	const char* op_cortas = "hvn:c:" ;

	/* Una estructura de varios arrays describiendo los valores largos */
	const struct option op_largas[] = {
		{ "help",         0,  NULL,   'h'},
		{ "version",      0,  NULL,   'v'},
		{ "lines",        1,  NULL,   'n'},
		{ "bytes",        1,  NULL,   'c'},
		{ NULL,           0,  NULL,   0  }
	};

	nombre_programa = argv[0];

	T_ProgramSettings settings = readOptions(argc, argv, op_cortas, op_largas);

	return settings;
}
