
#include "tp0.h"

char* leerLinea (FILE* fEntrada) {
	int i = 0;
	unsigned char c = fgetc(fEntrada);
	char* linea = NULL;
	if( !feof(fEntrada)) {
		linea = (char*) malloc (sizeof(char));
		linea[0] = 0;
		while ( (c != '\n') && (!feof(fEntrada))) {
			linea = (char*) realloc (linea, sizeof(char) * (i + 1));
			linea[i] = c;
			c = fgetc(fEntrada);
			i++;
		}
		linea = (char*) realloc (linea, sizeof(char) * (i + 2));
		if( !feof(fEntrada)) {
			linea[i] = c;
			linea[i+1] = 0;
		}
		else
			linea[i] = 0;
	}
	return linea;
}

char* leerByte (FILE* fEntrada) {
	char* byte = NULL;
	unsigned char c = fgetc(fEntrada);
	if( !feof(fEntrada)) {
		byte = (char*) malloc (sizeof(char) * 2);
		byte[0] = c;
		byte[1] = 0;
	}
	return byte;
}

void obtenerUltimosDesde(FILE* fEntrada, int requeridosDesde, int tipoLectura) {
	char* ultimoLeido = NULL;
	int cantLeido = 0;
	do {
		if(tipoLectura == 1)
			ultimoLeido = leerLinea(fEntrada);
		else if(tipoLectura == 3)
			ultimoLeido = leerByte(fEntrada);
		if(ultimoLeido != NULL) {
			cantLeido++;
			if(cantLeido >= requeridosDesde)
				printf("%s",ultimoLeido);
		}
	} while (ultimoLeido != NULL);
}

void obtenerUltimos(FILE* fEntrada, int cantRequerida, int tipoLectura) {
	char* bufferUltimos[cantRequerida];
	int i = 0;
	for(i=0; i< cantRequerida; i++)
		bufferUltimos[i] = NULL;
	char* ultimoLeido = NULL;
	int cantLeidos = 0;
	i = 0;
	do {
		if(tipoLectura == 0)
			ultimoLeido = leerLinea(fEntrada);
		else if(tipoLectura == 2)
			ultimoLeido = leerByte(fEntrada);
		if(ultimoLeido != NULL) {
			if(bufferUltimos[i] != NULL)
				free(bufferUltimos[i]);
			bufferUltimos[i] = ultimoLeido;
			i = ++cantLeidos % cantRequerida;
		}
	} while (ultimoLeido != NULL);
	
	if(cantLeidos > 0) {
		if(cantLeidos < cantRequerida)
			i = 0;
		
		while ( bufferUltimos[i] != NULL) {
			printf("%s", bufferUltimos[i]);
			free(bufferUltimos[i]);
			bufferUltimos[i] = NULL;
			i++;
			i = i % cantRequerida;
		}
	}
}

int imprimirLineas(FILE *fEntrada, T_ProgramSettings* settings) {
	if (settings->ToDo[0] == 1 || settings->ToDo[1] == 1) {
		if (settings->ToDo[0] == 1)
			obtenerUltimos(fEntrada, settings->quantity, 0);
		else if (settings->ToDo[1] == 1)
			obtenerUltimosDesde(fEntrada, settings->quantity, 1);
	} else if (settings->ToDo[2] == 1 || settings->ToDo[3] == 1) {
		if (settings->ToDo[2] == 1)
			obtenerUltimos(fEntrada, settings->quantity, 2);
		else if (settings->ToDo[3] == 1)
			obtenerUltimosDesde(fEntrada, settings->quantity, 3);
	}
	return 0;
}

int main(int argc, char** argv) {
	
	T_ProgramSettings settings = parseArguments(argc, argv);
	
	int returnCode = 0;
	int k = 0;
	for ( ; k < settings.paramsInCount ; k++) {
		
		if(settings.paramsInCount > 1) {
			if(k > 0)
				printf("\n");
			printf("==> %s <==\n", settings.paramsIn[k]);
		}
		
		FILE *fEntrada = fopen(settings.paramsIn[k],"r");
		if(fEntrada == NULL) {
			returnCode = 1;
			fprintf(stderr, "No se pudo abrir el archivo %s\n", settings.paramsIn[k]);
			continue;
		}

		imprimirLineas(fEntrada, &settings);

		fclose(fEntrada);
	}
	
	if(settings.paramsInCount == 0)
		imprimirLineas(stdin, &settings);

	return returnCode;
}
