#ifndef TP0_H_
#define TP0_H_

#include <stdio.h>
#include <memory.h>
#include <malloc.h>
#include "argParser.h"

char* leerLinea(FILE* fEntdada);

char* leerByte (FILE* fEntrada);

void obtenerUltimosDesde(FILE* fEntrada, int requeridosDesde, int tipoLectura);

void obtenerUltimos(FILE* fEntrada, int cantRequerida, int tipoLectura);

int imprimirLineas(FILE *fEntrada, T_ProgramSettings* settings);

#endif /*TP0_H_*/
