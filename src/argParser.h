
#ifndef ARGPARSER_H_
#define ARGPARSER_H_

#include <getopt.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX 2056
#define MAX_PARAMS_IN 255

typedef struct {
	int ToDo[4];
	char* paramsIn[MAX_PARAMS_IN];
	int paramsInCount;
	char cadenaCompleta[MAX];
	int quantity;
} T_ProgramSettings;

T_ProgramSettings parseArguments (int argc, char** argv) ;

#endif /* ARGPARSER_H_ */
