\documentclass[a4paper,11pt]{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Paquetes utilizados
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Graficos complejos
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{placeins}

% Soporte para el lenguaje español
\usepackage{textcomp}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\DeclareUnicodeCharacter{B0}{\textdegree}
\usepackage[spanish]{babel}

% Codigo fuente embebido
\usepackage{listings}

% PDFs embebidos para el apendice
\usepackage{pdfpages}

% Matematicos
\usepackage{amssymb,amsmath}

% Tablas complejas
\usepackage{multirow}

% Graficador
\usepackage{pgfplotstable}
\usepackage{pgfplots}

% Fromato MIPS para los archivos incluidos
% \usepackage{docs/mips}

% Formato de parrafo
\setlength{\parskip}{1ex plus 0.5ex minus 0.2ex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Titulo
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Titulo principal del documento.
\title{\textbf{Trabajo Práctico 0: Infraestructura básica}}

% Informacion sobre los autores.
\author{\\
  Alvez Federico, \textit{P. 87.625}                                \\
  \texttt{fmalvez@gmail.com}                                   \\ [2.5ex]
  Opromolla Giovanni, \textit{P. 87.761}                          \\
  \texttt{giopromolla@gmail.com}                                   \\ [2.5ex]
  Toniolo Nicolas, \textit{P. 87.316}                          \\
  \texttt{ntoniolo@gmail.com}                                       \\ [2.5ex]
                                                                   \\
  \normalsize{1er. Cuatrimestre de 2015}                           \\
  \normalsize{66.20 Organización de Computadoras}                  \\
  \normalsize{Facultad de Ingeniería, Universidad de Buenos Aires} \\
}
\date{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Documento
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

% ----------------------------------------------------------------------
% Top matter
% ----------------------------------------------------------------------
\thispagestyle{empty}
\maketitle

\begin{abstract}

  Este informe resume el desarrollo del trabajo practico 0 de la materia
  Organización de Computadoras (66.20) dictada en el primer cuatrimestre de
  2015 en la Facultad de Ingeniería de la Universidad de Buenos Aires. El mismo
  consiste en la construcción de un sistema minimalista del comando \textit{tail}
  de Unix, el cuál será compilado en gxemul y comparado en performance contra
  el comando original.

\end{abstract}

\clearpage

% ----------------------------------------------------------------------
% Tabla de contenidos
% ----------------------------------------------------------------------
\tableofcontents
\clearpage


% ----------------------------------------------------------------------
% Desarrollo
% ----------------------------------------------------------------------
\part{Desarrollo}

\section{Introducción}

  El objetivo de este trabajo es comparar el rendimiento que tienen dos
  implementaciones distintas del comando \textit{tail}
  escritos en lenguaje C.
  
  Principalmente se va a evaluar como cambia el rendimiento cuando al código se
  le aplican optimizaciones para que tenga una ventaja frente al original,
  siempre y cuando se ejecute el programa bajo el GxEmul que posee una
  arquitectura de MIPS 32.

\section{Implementación}

  El sistema que resuelve el problema planteado en el enunciado (disponible en
  el anexo~\ref{sec:enunciado}) fue implementado en su totalidad en lenguaje C,
  y se puso en funcionamiento bajo la máquina virtual de MIPS como lo indicaba
  el enunciado.  La diferencia fundamental radica en las optimizaciones sobre
  la implementación a través del algoritmo de \textit{tail} desarrollado. El
  código fuente de la solución esta disponible en el anexo~\ref{sec:source}.

%  Cabe destacar que hemos realizado mejoras respecto de la versión inicial
%  tales como modificar el código fuente para poder reducir los tiempos de
%  procesamiento del objetivo del programa.

\FloatBarrier

\section{Compilación}\label{sec:compilacion}

  Se instrumentó un \textit{makefile} para ejecutar las instrucciones adecuadas
  de compilación para los distintos escenarios requeridos. La tarea
  \textit{make} compila individualmente cada uno de los archivos fuente de
  extensión \textit{c} a través del ejecutable \textit{gcc}.  Los
  comandos utilizados para compilar cada uno de estos archivos fuente son los
  siguientes:

\begin{lstlisting}
  gcc -c -o build/obj/argParser.o 
    src/argParser.c -I./src -Wall 
  gcc -c -o build/obj/tp0.o 
    src/tp0.c -I./src -Wall 
\end{lstlisting}

  De este listado, cada una de las invocaciones obedece a la siguiente estructura
  de argumentos:

\begin{description}

  \item[-c] Compila o ensambla el código fuente pero no corre el linker.  Por
    lo tanto la salida corresponde a un archivo objeto por cada archivo fuente.

  \item[-o] Especifica cual sera el archivo de salida sea este un archivo
    objeto, ejecutable, ensamblado o código preprocesado de C.

  \item[-Wall] Activa todos los mensajes de warning.

  \item[-I] Agrega el directorio especificado a la lista de directorios
    buscados para los archivos header

  \item[-pg] (opcional) Genera código extra para escribir información acerca
    del profiling del programa evaluado a través de \textit{gprof}. Ésta opción será
    utilizada al momento de analizar los tiempos.

\end{description}

  El resultado de la ejecución de estos comandos es que se generan archivos
  objeto para cada fuente, listos para ser linkeados, en el directorio
  \textit{build/obj}. Para realizar este último paso, se invoca nuevamente a
  \textit{gcc} con un ultimo comando:

\begin{lstlisting}
  gcc -o build/tp0 
  build/obj/argParser.o 
  build/obj/tp0.o 
  -I./src -Wall
\end{lstlisting}

  El comando linkea todos los archivos objeto en un ejecutable final,
  \textit{build/tp0}.

\section{Análisis de tiempos de ejecución}

  En las siguientes secciones detallaremos el proceso de análisis de tiempo de
  ejecución realizado para los diferentes casos.

\subsection{Análisis previo}\label{sec:tiempos}

  El análisis de tiempos de ejecución se focalizará en comparar la versión de
  la implementación nuestra de tail frente a la oficial de gxemul, 
  puestas a prueba bajo el mismo sistema.

  Se comparará el desempeño de los algoritmos utilizando la herramienta de
  \textit{gprof} y \textit{time}. La herramienta \textit{gprof} es un evaluador
  del progfiling del programa.  Profiling, es un aspecto importante de la
  programación de software.  A través de los perfiles se pueden determinar las
  partes de código de un programa que demandan mucho tiempo y necesitan ser
  re-escrita. Esto ayuda a que su ejecución sea más rápido que la que se desea.
  Mientas que la herramienta \textit{time} corre el programa y resume en
  tiempos la utilización de los recursos del sistema, distinguiendose entre
  ellos el tiempo dedicado por el sistema y el tiempo dedicado por el usuario.

  Para poder ejecutar \textit{gprof} debemos recompilar el código nuestro
  con la opción de \textit{-pg} como se mencionó en la sección de
  ~\ref{sec:compilacion}, luego correr el programa para poder generar el
  archivo necesario para profiling \textit{gmon.out} y finalmente correr
  \textit{gprof} con el programa y el \textit{gmon.out} como argumentos para
  poder evaluar los resultados mediante el comando.

  \subsection{Experimentos con \textit{time}}

  Se realizaron pruebas utilizando los archivos provistos por la cátedra sobre un
  entorno MIPS 32 emulado por GXemul.

  Se ejecutaró cada binario con \textit{time} 5 veces por cada muestra disponible y se promediaron los valores obteniendo:

\FloatBarrier

\begin{table}[h!t]
\centering
\begin{tabular}{|c|c|c|}
\hline
Tamaño de entrada & \begin{tabular}[c]{@{}c@{}} Tiempo Ejecución \\ Promedio tp0 [s]\end{tabular} & \begin{tabular}[c]{@{}c@{}} Tiempo Ejecución \\ Promedio tail [s]\end{tabular} \\ \hline
$100*2^{0}$       & 0,088                                 & 0,072                                  \\ \hline
$100*2^{1}$       & 0,082                                 & 0,064                                  \\ \hline
$100*2^{2}$       & 0,072                                 & 0,06                                   \\ \hline
$100*2^{3}$       & 0,092                                 & 0,072                                  \\ \hline
$100*2^{4}$       & 0,116                                 & 0,08                                   \\ \hline
$100*2^{5}$       & 0,17                                  & 0,07                                   \\ \hline
$100*2^{6}$       & 0,254                                 & 0,07                                   \\ \hline
$100*2^{7}$       & 0,416                                 & 0,07                                   \\ \hline
$100*2^{8}$       & 0,7                                   & 0,084                                  \\ \hline
$100*2^{9}$       & 1,404                                 & 0,076                                  \\ \hline
$100*2^{10}$      & 2,708                                 & 0,054                                  \\ \hline
$100*2^{11}$      & 5,28                                  & 0,076                                  \\ \hline
$100*2^{12}$      & 10,414                                & 0,072                                  \\ \hline
$100*2^{13}$      & 20,91                                 & 0,058                                  \\ \hline
$100*2^{14}$      & 41,642                                & 0,06                                   \\ \hline
$100*2^{15}$      & 82,08                                 & 0,068                                  \\ \hline
$100*2^{16}$      & 163,124                               & 0,08                                   \\ \hline
$100*2^{17}$      & 330,622                               & 0,072                                  \\ \hline
Total             & 660,17                                & 1,26                                   \\ \hline
\end{tabular}
\caption{Tiempos promedio de 5 corridas por entrada}
\label{tab:resultados}
\end{table}

 Debido a la gran diferencia de magnitudes entre los tiempos de los binarios comparados, 
 vamos a aplicar una escala logaritmica para graficarlos y poder apreciar las diferencias.

\begin{tikzpicture}
	\begin{axis}[
		height=9cm,
		width=9cm,
		grid=major,
		xlabel={Tamaño muestra[log(bytes)]},
    		ylabel={Tiempo Insumido[log(s)]},
		title={Tiempo vs Tamaño}
	]
		
%	\addplot {-x^5 - 242};
%	\addlegendentry{model}

	\addplot coordinates {
(2,-1.0555)
(2.3010,-1.0862)
(2.6021,-1.1427)
(2.9031,-1.0362)
(3.2041,-0.9355)
(3.5051,-0.7696)
(3.8062,-0.5952)
(4.1072,-0.3809)
(4.4082,-0.1549)
(4.7093,0.1474)
(5.0103,0.4326)
(5.3113,0.7226)
(5.6124,1.0176)
(5.9134,1.3204)
(6.2144,1.6195)
(6.5154,1.9142)
(6.8165,2.2125)
(7.1175,2.5193)
	};
	\addlegendentry{tp0}

	\addplot coordinates {
(2,-1.1427)
(2.3010,-1.1938)
(2.6021,-1.2218)
(2.9031,-1.1427)
(3.2041,-1.0969)
(3.5051,-1.1549)
(3.8062,-1.1549)
(4.1072,-1.1549)
(4.4082,-1.0757)
(4.7093,-1.1192)
(5.0103,-1.2676)
(5.3113,-1.1192)
(5.6124,-1.1427)
(5.9134,-1.2366)
(6.2144,-1.2218)
(6.5154,-1.1675)
(6.8165,-1.0969)
(7.1175,-1.1427)
	};
	\addlegendentry{tail}

	\end{axis}
\end{tikzpicture}

\FloatBarrier

  En el gráfico podemos ver claramente como nuestra implementacion aumenta su tiempo 
  de ejecucion de manera exponencial mientras que el tail del sistema operativo se mantiene
  estable a medida que el tamaño de muestra aumenta. Creemos que los tiempos menores alcanzados
  con entradas mayores por el programa tail se deben a que las pruebas se ejecutan secuencialmente 
  y el sistema operativo pudo haber utilizado recursos en procesos ajenos a los probados.

  En función de los resultados obtenidos frente a los tiempos observados para cuando se analizan
  todos los archivos juntos, destacamos que el speedup es:

{\centering  SpeedUp = $\dfrac{1.26s}{660.17s}$ = 0,0019

}

\FloatBarrier

  \subsection{Experimentos con \textit{gprof}}

  Para poder entender como leer el archivo de análisis de \textit{gprof} se
  debe tener en cuenta el significado de cada uno de los valores que se tiene
  sobre las columnas.

\begin{description}
  
  \item[\% time] Indica el porcentaje de la duración total de
    tiempo de la función dentro del programa.

  \item[cumulative seconds] La suma de la cantidad de segundos acontecidos por
    dicha función respecto de las que se encuentran listadas.

  \item[self seconds] La cantidad de segundos acontecidos solamente por dicha
    función.

  \item[calls] El número de veces que la función fue invocada.

  \item[self s/call] La cantidad promedio de milisegundos consumidos por dicha
    función.
  
  \item[total s/call] La cantidad promedio de milisegundos consumidos por dicha
    función y sus descendientes por llamada.

  \item[name] El nombre de la función.

\end{description}

Para la primera implementación tenemos los siguientes resultados:

\lstset{
  columns=flexible
}

Si desea ver los resultados completos por favor diríjase al Apéndice~\ref{sec:bruto}.

\FloatBarrier

%Mientras que para la segunda implementación tenemos:

%Si desea ver los resultados completos por favor diríjase al Apéndice~\ref{sec:bruto}.

%\FloatBarrier

  \subsection{Análisis posterior}

  En base al análisis de los resultados de gprof, pudimos observar que
  los valores mas altos o que mas afectan al rendimiento del sistemas son, en
  primer lugar el método fopen. Dicho método es utilizado para abrir el archivo
  a procesar. Esta parte del código es difícil de optimizar dado que, como se
  puede observar en el informe de análisis sólo es utilizado una vez y no puede 
  ser reemplazo por otro método.
  
  En consecuencia se decidió analizar otras parte del código que sea posible optimizar.
  
  Un dato sobresaliente dentro de los informes presentados es la aparición
  del método fgetc, dicho método es utilizado para leer un caracter. En
  consecuencia la cantidad de llamados a dicho método depende directamente de
  la cantidad de caracteres que tenga el archivo que debemos analizar. Si el
  archivo no es de gran tamaño esto podría no generar un impacto, dado que la
  cantidad de caracteres no seria significativa, sin embargo en archivo grandes
  toma una relevancia importante. Como puede observarse en cada uno de los
  análisis presentados la cantidad de veces que se llama a dicho método va de
  202 veces hasta 13107202. Obviamente que si utilizáramos archivos mas
  grandes, esta cantidad seguiría creciendo, una vez por cada caracter nuevo.
  Como sabemos, no es posible cambiar el tiempo que esta función demora, sin
  embargo, hemos analizado la posibilidad de disminuir la cantidad de llamados
  que se realizan a dicho método, o mejor aún realizar un cambio en nuestro
  código que nos permita reemplazar dicha función con otra que requiera de una
  cantidad menor de llamados. 

\section{Conclusiones}

  En base a los resultados optenidos de ejecutar la herramienta gprof nuevamente
  pero sobre la implementación que incluía el código de optimización planteado
  anteriormente podemos confirmar lo que preveíamos. La cantidad de acceso que
  realizados sobre el método fgets se redujo considerablemente en comparación a
  los accesos al método fgetc. En promedio podríamos decir que se accedió al
  método fgets el 1\% de las veces que se accedía al método fgetc. Esto permite
  tener un tiempo de ejecución del método inferior y a su vez una disminución en
  el tiempo de ejecución total del sistema.
  
  Claramente la diferencia se hace mas notoria cuanto mas grande sea el archivo o
  cuanto mas caracteres haya en una misma linea, dado que la cantidad de
  caracteres crece mucho mas rápido en comparación con la cantidad de lineas del
  archivo.
  
  En necesario, para validar lo anteriormente dicho que existen casos
  particulares para los cuales la optimización es verdadera.  Uno de esos casos
  es cuando cada linea del archivo solo contiene una letra, en ese caso la
  cantidad de llamados a cada método será la misma y como consecuencia no habría
  optimización alguna.
  
%  Por otra parte podría ocurrir algo similar en el caso de que el archivo
%  contenga en su mayoría lineas en blanco.  Mas haya de estos dos casos
%  particulares, en el resto de los casos la optimización seria real, y como
%  consideramos que siempre conviene optimizar el caso mas común creemos que el
%  cambio realizado fue valioso. 

%  Respecto a los tiempos de medición, podemos concluir que sin ningún lugar a
%  dudas usaríamos las segunda implementación sobre el algoritmo crudo ya que el
%  speed up obtenido es mayor a 1.

  Respecto a los tiempos de medición, podemos concluir que sin ningún lugar a
  dudas usaríamos las implementación oficial sobre la del algoritmo crudo ya que el
  speed up obtenido es mucho menor a 1.

\clearpage

\part{Apéndice}
\appendix

\section{Enunciado original}\label{sec:enunciado}
\includepdf[pages={-}]{tp0-2015-1.pdf}

\clearpage
\section{Código fuente}\label{sec:source}

% Si desea ver el código completo, revisar el documento digital.
% \clearpage

\definecolor{gray}{rgb}{0.5,0.5,0.5}
\lstset{%
  title=\lstname,
  language=C,
  basicstyle=\footnotesize,
  showspaces=false,
  showstringspaces=false,
  breaklines=true,
  commentstyle=\color{gray},
  numbers=left,
  numberstyle=\tiny\color{gray},
  numbersep=5pt,
  frame=single,
  extendedchars=\true,
  inputencoding=utf8
}

\lstinputlisting{../src/tp0.c}
\lstinputlisting{../src/tp0.h}
\lstinputlisting{../src/argParser.c}
\lstinputlisting{../src/argParser.h}

\clearpage
\section{Toma de tiempos en bruto}\label{sec:bruto}
%\lstinputlisting{docs/analysis_1-short.txt}

  Tiempos con el comando \textit{time}:

\lstinputlisting{../benchmarkResult.txt}

\clearpage

  Detalle de uso con el comando \textit{gprof}:

\lstinputlisting{analysis_2.txt}

\end{document}
