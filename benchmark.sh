#!/bin/bash

measureTime () {
	echo "Ejecutando: $1 $2"
	echo "\nEjecutando: $1 $2" >> $4
	REAL_TIMES=""
	USER_TIMES=""
	SYS_TIMES=""
	for j in `seq 1 5`;
	do
		TIEMPOS=`(time -p $1 $2) 2>&1 >/dev/null`
		REAL_TIMES="${REAL_TIMES} $(echo $TIEMPOS | awk '{print $2}')"
		USER_TIMES="${USER_TIMES} $(echo $TIEMPOS | awk '{print $4}')"
		SYS_TIMES="${SYS_TIMES} $(echo $TIEMPOS | awk '{print $6}')"
		echo "Corrida $j:\n$TIEMPOS" >> $4
	done
	echo "$1 $2 REAL ${REAL_TIMES}" >> $3
	echo "$1 $2 USER ${USER_TIMES}" >> $3
	echo "$1 $2 SYS ${SYS_TIMES}" >> $3
}

NOW=`date +%Y%m%d-%H%M%S`
RESULT_FILE="benchmarkResult${NOW}.txt"
RAW_RESULT_FILE="benchmarkResult${NOW}_raw.txt"

TEST_CMD="./build/tp0 -n10"
TAIL_CMD="tail -n10"

echo "" > $RESULT_FILE
(time -p ls -l) 2>&1 >/dev/null | xargs | awk '{print "soft data timeType " $1, $3, $5}' >> $RESULT_FILE

for i in `seq 0 16`;
do
	n=`printf %02d $i`
	INPUT_FILE="./data/prueba-$n"
	measureTime "${TEST_CMD}" "${INPUT_FILE}" "${RESULT_FILE}" "${RAW_RESULT_FILE}"
	measureTime "${TAIL_CMD}" "${INPUT_FILE}" "${RESULT_FILE}" "${RAW_RESULT_FILE}"
done

