#!/bin/bash

NOW=`date +%Y%m%d-%H%M%S`
RESULT_FILE="docs/analysys_${NOW}.txt"

TEST_CMD="./build/tp0 -n10"
GPROF_CMD="gprof ./build/tp0 gmon.out"

echo "" > $RESULT_FILE

for i in `seq 0 16`;
do
	n=`printf %02d $i`
        INPUT_FILE="./data/prueba-$n"

	echo -e "Ejecutando: $TEST_CMD $INPUT_FILE"
	echo -e "\nEjecutando: $TEST_CMD $INPUT_FILE" >> $RESULT_FILE
	$TEST_CMD $INPUT_FILE >> /dev/null
	$GPROF_CMD >> $RESULT_FILE
done

